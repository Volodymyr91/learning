package root;

import java.io.IOException;

public class Utils {
    public static int inputNumberFromConsole() {
        try {
            return System.in.read() - '0';
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
