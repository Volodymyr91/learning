package root;

public class LinkedList {
    private Node head;

    public void add(int number) {
        if (head == null) {
            head = Node.obtain(number);
        }

        Node currentNode = head;

        while (currentNode.next != null) {
            currentNode = currentNode.next;
        }

        currentNode.next = Node.obtain(number);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[ ");
        Node currentNode = head;

        while (currentNode != null) {
            builder.append(currentNode.number + " ");
            currentNode = currentNode.next;
        }

        builder.append(" ]");
        return builder.toString();
    }

    private static class Node {
        private int number;
        private Node next;

        private static Node obtain(int number) {
            Node node = new Node();
            node.number = number;
            return node;
        }
    }
}
