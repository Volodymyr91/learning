package root;

public class Application {
    public static void main(String[] args) {
        Command command;

        LinkedList list = new LinkedList();

        while ((command = Command.getMenuItem()) != Command.QUIT) {
            switch (command) {
                case ADD_NUMBER:
                    list.add(Utils.inputNumberFromConsole());
                    break;
            }

            System.out.println(list);
        }
    }

    private enum Command {
        ADD_NUMBER,
        QUIT;

        private static final Command[] VALUES = values();

        public static Command getMenuItem() {
            System.out.println("Enter the number of your choice: ");

            for (Command command : VALUES) {
                System.out.println(command.ordinal() + " - " + command.name().toLowerCase());
            }

            return inputMenuItem();
        }

        private static Command inputMenuItem() {
            for (;;) {
                int selection = Utils.inputNumberFromConsole();

                if (selection < 0 || selection >= VALUES.length) {
                    System.out.println("incorrect input");
                } else {
                    return VALUES[selection];
                }
            }
        }
    }
}
